<?php

class ArrayHelper {

    /**
     * Get a key from an array.
     * 
     * @param array $array
     * @param string $name
     * @return mixed
     */
    public static function get(array $array, $name, $default = null) {
        $name = explode('.', $name);
        foreach ($name as $namePart) {
            if (!is_array($array)) {
                return $default;
            }elseif (!isset($array[$namePart])) {
                return $default;
            } 
            $array = $array[$namePart];
        }
        return $array;
    }

    /**
     * Set a value in an array.
     * 
     * @param array $array
     * @param string $name
     * @return boolean Succes
     */
    public static function set(array &$array, $name, $value) {
        $name = explode('.', $name);
        foreach ($name as $namePart) {
            if (!is_array($array)) {
                return false;
            }elseif (!isset($array[$namePart])) {
                return false;
            } 
            $array = &$array[$namePart];
        }
        $array = $value;
        return true;
    }

}
