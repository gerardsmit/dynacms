<?php
abstract class Model {
    /**
     * The table that this model is using.
     * 
     * @var string|null
     */
    public $tableName = null;
    
    /**
     * The table with all the data.
     * @var array
     */
    public $data = array();
    
    /**
     * When something changed, this boolean will be set on true.
     * After a save, it will be false.
     * @var boolean
     */
    public $changed = false;
    
    public function __construct() {
        
    }
    
    public function __get($name) {
        return isset($this->data[$name]) ? $this->data[$name] : null;
    }
    
    public function __set($name, $value) {
        $this->data[$name] = $value;
        $this->changed = true;
    }
}