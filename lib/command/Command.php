<?php
abstract class Command {
    
    /**
     * Called when the console uses this command.
     * 
     * @param array $args The arguments.
     */
    abstract function invoke($args);
    
}