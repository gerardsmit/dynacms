<?php
App::uses('Command', 'Command');

class CreateCommand extends Command {
    
    /**
     * Called when the console uses this command.
     * 
     * @param array $args The arguments.
     */
    public function invoke($args) {
        // The temporary names.
        $tempType = array_shift($args);
        $tempName = array_shift($args);
        
        // Check if they are both set. Else show the usage.
        if(empty($tempType) || empty($tempName)) {
            Console::write('Usage: cms create [type] [name]', 'cyan');
            return;
        }
        
        // Markup the temporary names.
        $type = ucfirst(strtolower($tempType));
        $name = ucfirst(strtolower($tempName));
        
        // Check if the type does exists.
        if(!isset(App::$types[$type])) {
            Console::write('Invalid type ' . $type, 'yellow');
            return;
        }
        
        // Get the content for the file.
        $data = $this->getContent($type, $name);
        
        // Get the location of the new file.
        $filePath = 'app/' . App::$types[$type] . '/' . $this->getFilename($type, $name) . '.php';
        
        // Check if the files does exists, because we don't want to overwrite anything.
        if(file_exists($filePath)) {
            Console::write('The name ' . $name . ' for the type ' . $type . ' is already used.', 'yellow');
            return;
        }
        
        // Put all the content in the file.
        file_put_contents($filePath, $data);
        Console::write($type . ' succesfully created.', 'green');
    }
    
    /**
     * Get the content for the given type and name.
     * 
     * @param string $type
     * @param string $name
     * @return string
     */
    private function getContent($type, $name) {
        switch($type) {
            case 'Model':
                return "<?php\nApp::uses('{$type}', '{$type}');\n\nclass {$name} extends {$type} {\n    \n}";
                
            case 'Vendor':
                return "<?php\nclass {$name} {\n    \n}";
            
            case 'Command':
                return "<?php\nApp::uses('{$type}', '{$type}');\n\nclass {$name}{$type} extends {$type} {\n\n    public function invoke(\$args) {\n        \n    }\n\n}";
            
            default:
                return "<?php\nApp::uses('{$type}', '{$type}');\n\nclass {$name}{$type} extends {$type} {\n    \n}";
        }
    }

    /**
     * Get the filename for the given type and name.
     * 
     * @param string $type
     * @param string $name
     * @return string
     */
    public function getFilename($type, $name) {
        switch($type) {
            case 'Model':
            case 'Vendor':
                return $name;
                
            default:
                return $name . $type;
        }
    }

}