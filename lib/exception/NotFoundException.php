<?php
namespace Dyna\Exceptions;

/**
 * The NotFoundException
 *
 * @author Gerard
 */
class NotFoundException extends Exception {
    
}
