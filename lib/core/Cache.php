<?php
class Cache {
    
    public static function remember($name, $object) {
        $data = serialize($object);
        file_put_contents('lib/tmp/' . $name, $data);
    }
    
    public static function forget($name) {
        unlink('lib/tmp/' . $name);
    }
    
}