<?php
class View {
    
    public $path = null;
    
    /**
     * 
     * @param string $path
     */
    public function __construct($path) {
        $this->path = $path;
    }
    
    /**
     * Load an helper.
     * @param string $helperName
     */
    public function loadHelper($helperName) {
        $className = $helperName . 'Helper';

        if(App::uses('Helper', $className)) {
            $helper = new $className;
            $this->{$helperName} = $helper;
        }
    }
    
    /**
     * Render the view.
     * 
     * @param string|null $path The path to the view
     * @param array $args All the parameters for the view.
     */
    public function render($args = array()) {
        // Loop through the arguments.
        foreach($args as $key => $value) {
            // Set the variable.
            $$key = $value;
        }
        
        include $this->path;
    }
    
}