<?php
App::uses('Utils', 'ArrayHelper');

class Config {
    private static $config;

    public static function load() {
        self::$config = json_decode(file_get_contents('app/config/app.json'), true);
    }
    
    public static function save() {
        file_put_contents('app/config/app.json', json_encode(self::$config, JSON_PRETTY_PRINT));
    }
    
    public static function get($name, $default = null) {
        return ArrayHelper::get(self::$config, $name, $default);
    }
    
    public static function set($name, $value) {
        ArrayHelper::set(self::$config, $name, $value);
    }

}