<?php

class Console {

    /**
     * All the console colors.
     * 
     * @source https://github.com/kevinlebrun/colors.php/blob/master/src/Colors/Color.php#L17
     * @var array
     */
    private static $styles = array(
        'reset' => '0',
        'bold' => '1',
        'dark' => '2',
        'italic' => '3',
        'underline' => '4',
        'blink' => '5',
        'reverse' => '7',
        'concealed' => '8',
        'default' => '39',
        'black' => '30',
        'red' => '31',
        'green' => '32',
        'yellow' => '33',
        'blue' => '34',
        'magenta' => '35',
        'cyan' => '36',
        'light_gray' => '37',
        'dark_gray' => '90',
        'light_red' => '91',
        'light_green' => '92',
        'light_yellow' => '93',
        'light_blue' => '94',
        'light_magenta' => '95',
        'light_cyan' => '96',
        'white' => '97',
        'bg_default' => '49',
        'bg_black' => '40',
        'bg_red' => '41',
        'bg_green' => '42',
        'bg_yellow' => '43',
        'bg_blue' => '44',
        'bg_magenta' => '45',
        'bg_cyan' => '46',
        'bg_light_gray' => '47',
        'bg_dark_gray' => '100',
        'bg_light_red' => '101',
        'bg_light_green' => '102',
        'bg_light_yellow' => '103',
        'bg_light_blue' => '104',
        'bg_light_magenta' => '105',
        'bg_light_cyan' => '106',
        'bg_white' => '107',
    );
    
    public static function reset() {
        echo "\033[0m";
    }
    
    public static function set($style) {
        echo "\033[" . self::$styles[$style] . "m";
    }

    public static function write($message, $style = null) {
        echo date('H:i:s') . ' - ';
        if(isset($style)) {
            echo "\033[" . self::$styles[$style] . "m" . $message . "\033[0m\n";
        }else{
            echo $message . "\n";
        }
    }

    public static function writeLogo() {
        echo " ____                     ____ __  __ ____  \n|  _ \ _   _ _ __   __ _ / ___|  \/  / ___| \n| | | | | | | '_ \ / _` | |   | |\/| \___ \ \n| |_| | |_| | | | | (_| | |___| |  | |___) |\n|____/ \__, |_| |_|\__,_|\____|_|  |_|____/ \n       |___/                     Version 0.1\n\n";
    }

}
