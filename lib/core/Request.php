<?php
App::uses('Lib.Component', 'Component');

/**
 * The request of the client, but formatted.
 *
 * @author Gerard
 * @version 0.1
 * @category Core
 */
class Request {
    public $request, $headers, $browser, $browserVersion;
    private $types;
    
    public function __construct() {
        $this->types = array();
        $this->init();
    }
    
    /**
     * Load the Request class.
     */
    private function init() {
        // Get the headers.
        $this->headers = getallheaders();
        if(isset($_GET['request'])) {
            $this->request = $_GET['request'];
        }
        
        // Add the types to the array.
        array_push($this->types, strtolower($_SERVER['REQUEST_METHOD']));
        
        // Check if this is an xmlhttprequest.
        if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && 
                strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
            array_push($this->types, 'ajax');
        }
        
        // Get the browser from the headers.
        $matches = array();
        preg_match('/(Chrome|Firefox|Safari|MSIE|Trident|Opera).?([0-9\.]*)/', $this->headers['User-Agent'], $matches);
        
        if(count($matches) > 0) {
            $this->browser = $matches[1];
            $this->browserVersion = floatval($matches[2]);
        }else{
            $this->browser = 'unknown';
            $this->browserVersion = 0;
        }
    }
    
    /**
     * Check if the request is the given type.
     * @param string $type
     * @return bool
     */
    public function is($type) {
        return in_array(strtolower($type), $this->types);
    }
    
    /**
     * Check if the request is through ajax.
     * @return bool
     */
    public function isAjax() {
        return $this->is('ajax');
    }
    
    /**
     * Check if the request-type is POST
     * @return bool
     */
    public function isPost() {
        return $this->is('post');
    }
    
    /**
     * Check if the request-type is GET
     * @return bool
     */
    public function isGet() {
        return $this->is('get');
    }
}
