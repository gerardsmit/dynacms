<?php
App::uses('Lib.Core', array('Request', 'Config', 'Cache'));

/**
 * The hearth of the CMS.
 *
 * @author Gerard
 * @version 0.1
 * @category Core
 */
class Dyna {
    /**
     * The request.
     * @var Request
     */
    protected static $request;
    
    /**
     * The PDO connection.
     * @var PDO
     */
    protected static $pdo;
    
    /**
     * The folder where the CMS is located.
     * @example 'CMS' if the fullpath is 'C:/xampp/htdocs/CMS'
     * @var string
     */
    public static $folder;
    
    /**
     * Called when the CMS starts.
     */
    public static function init() {
        Config::load();
    }
    
    /**
     * Handle a HTTP-request.
     */
    public static function handleRequest() {
        // Create the request.
        self::$request = new Request();
        
        // Set the folder where the CMS is in.
        self::$folder = substr(getcwd(), strlen($_SERVER["DOCUMENT_ROOT"]) + 1);
        
        // Create URL
        $url = isset($_GET['request']) ? explode('/', filter_var(rtrim($_GET['request'], '/'), FILTER_SANITIZE_URL)) : array();
        
        // Get the controller and the action from the URL.
        $controllerNameTemp = array_shift($url);
        $actionTemp = array_shift($url);
        
        // Markup the controller-name and the action-name.
        $controllerName = (empty($controllerNameTemp) ? 'Pages' : ucfirst(strtolower($controllerNameTemp)));
        $className = $controllerName . 'Controller';
        $action = empty($actionTemp) ? 'index' : strtolower($actionTemp);
        
        // Check if the controller exists
        if(App::uses('Controller', $className)) {
            $controller = new $className;
            call_user_func_array(array($controller, $action), $url);
            
            if($controller->autoView) {
                $controller->render($action);
            }
        }else{
            // TODO Make a 404-page
            echo 'NotFound';
        }
    }
    
    /**
     * Handle a Console-command.
     * @param array $argv The arguments.
     */
    public static function handleConsole($argv) {
        App::uses('Lib.Core', 'Console');
        
        // Write the fancy logo.
        Console::writeLogo();
        
        // Shift the first parameter since it's just the path to console.php.
        array_shift($argv); 
        
        // Format the command.
        $commandName = ucfirst(strtolower(array_shift($argv)));
        $className = $commandName . 'Command';
        
        // Check if we can include the command.
        if(App::uses('Command', $className)) {
            // Found 'em! Let's invoke the command.
            $command = new $className;
            $command->invoke($argv);
        }else{
            // The command is not found...
            Console::write('The command ' . $commandName . ' is not found!', 'yellow');
        }
    }
    
    /**
     * Returns the PDO.
     * @return PDO
     */
    public static function getPDO() {
        if(!isset(self::$pdo)) {
            self::$pdo = new PDO('mysql:host=' . Config::get('database.host') . ';dbname=' . Config::get('database.database'), Config::get('database.username'), Config::get('database.password'));
        }
        return self::$pdo;
    }
}