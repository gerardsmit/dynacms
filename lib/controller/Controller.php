<?php
App::uses('Lib.Core', 'View');

/**
 * The default controller.
 * @author Gerard
 */
class Controller {
    
    /**
     * Should Dyna render the vieuw automatically?
     * @var boolean 
     */
    public $autoView = true;
    
    /**
     * All the helpers that this controller uses.
     * @var array 
     */
    public $helpers = array('Form');
    
    public function __construct() {
        $className = get_class($this);
        $this->name = substr($className, 0, strpos($className, 'Controller'));
    }
    
    public function render($name) {
        $path = App::find('View', $name, $this->name);
        $view = new View($path);
        
        foreach($this->helpers as $helper) {
            $view->loadHelper($helper);
        }
        
        $this->autoView = false;
        echo $view->render();
    }
    
}
