<?php

class App {

    /**
     * All the types and the path to it.
     * 
     * @var array
     */
    public static $types = array(
        // Type                Path
        'Controller' => 'controller',
        'Component' => 'controller\component',
        'Exception' => 'exception',
        'Model' => 'model',
        'View' => 'view',
        'Exception' => 'exception',
        'Vendor' => 'vendor',
        'Command' => 'command',
        'Core' => 'core',
        'Utils' => 'utils',
        'Helper' => 'controller\helper'
    );

    /**
     * All the folder-names and the path to it.
     * 
     * NOTE: App::find goes from up to down, so if the file is found in App,
     * the rest will not be searched.
     * 
     * @var array 
     */
    public static $folders = array(
        // Name               Path
        'App' => 'app',
        'Lib' => 'lib'
    );

    /**
     * Find the path of the class and name.
     * 
     * @param string $type
     * @param string $name
     * @return string|null
     */
    public static function find($type, $name, $insideFolder = '') {
        // Look for the dot in the class.
        $strPos = strpos($type, '.');

        // Check if the folder is not empty.
        if (!empty($insideFolder)) {
            // Add a directory separator after it.
            $insideFolder .= DS;
        }

        // Check if the found the dot.
        if ($strPos) {
            // The dot is in the class, so the file is inside a folder.
            // Locate the file and check if it exists.
            $folder = self::$folders[substr($type, 0, $strPos)];
            $classPath = self::$types[substr($type, $strPos + 1)];
            $path = $folder . DS . $classPath . DS . $insideFolder . $name . '.php';

            if (file_exists($path)) {
                // Found the file, return the path.
                return $path;
            }
        } else {
            // There is no dot in the class, so we're going to scan all the directories.
            // For making a faster site. Give the folder in the class.
            $classPath = self::$types[$type];

            foreach (self::$folders as $folder) {
                $path = $folder . DS . $classPath . DS . $insideFolder . $name . '.php';
                if (file_exists($path)) {
                    // Found the file, return the path.
                    return $path;
                }
            }
        }

        // Not found. Return null.
        return null;
    }

    /**
     * Require a file from the CMS.
     * 
     * @param string $type
     * @param string $name
     * @return boolean
     */
    public static function uses($type, $name, $insideFolder = '') {
        // Try to find the path.
        if (is_array($name)) {
            $count = 0;
            foreach ($name as $actualName) {
                // Check if the class is already loaded.
                if (class_exists($actualName)) {
                    continue;
                }
                
                $path = App::find($type, $actualName, $insideFolder);

                // Check if the path if found.
                if (isset($path)) {
                    // Its found, require the file and return true.
                    require $path;
                    $count++;
                }
            }
            return $count;
        } else {
            // Check if the class is already loaded.
            if (class_exists($name)) {
                return true;
            }
            
            $path = App::find($type, $name, $insideFolder);

            // Check if the path if found.
            if (isset($path)) {
                // Its found, require the file and return true.
                require $path;
                return true;
            } else {
                // Path not found, return false.
                return false;
            }
        }
    }

}
