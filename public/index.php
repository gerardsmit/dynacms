<?php
chdir('..');

require 'lib/bootstrap.php';
require 'lib/app.php';
require 'lib/dyna.php';

Dyna::init();
Dyna::handleRequest();